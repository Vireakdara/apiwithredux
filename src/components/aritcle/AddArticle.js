import { Form, Button } from "react-bootstrap";
import React, { Component } from "react";
import { connect } from "react-redux";
import Radio from "./Radio";
import strings from "../locale/string";
import { getCategory } from "../../redux/actions/category/categoryAction";
import queryString from "query-string";
import {
  addArticle,
  viewArticle,
  editArticle,
  uploadImg,
} from "../../redux/actions/article/articleAction";


class AddArticle extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      description: "",
      imgFile: "",//upload img file
      imgLink: "",//upload img link file
      isUpdate: false,
      updateId: "",// update
      categoryId: 0, //
      titleBlank: "",// validation
      descriptionBlank: "",// validation
      
    };
    this.handleChangeImage = this.handleChangeImage.bind(this);
  }

  componentWillMount() {
    this.props.getCategory();
    let QSdata = queryString.parse(this.props.location.search);
    if (QSdata.id !== undefined) {
      viewArticle(QSdata.id, (data) => {
        this.setState({
          updateId: QSdata.id,
          title: data.title,
          description: data.description,
          categoryId: data.category ? data.category._id : null,
          isUpdate: Boolean(QSdata.update),
          imgLink: data.image,
        });
      });
    }
  }


  
// -------------------------------------------------------------------
//                  Upload IMG
// -------------------------------------------------------------------
  handleChangeImage = (e) => {
    this.setState({
      imgFile: e.target.files[0],
      imgLink: URL.createObjectURL(e.target.files[0]),
    });
  };



  
// -------------------------------------------------------------------
//                  Add New Article
// -------------------------------------------------------------------
  newArticle = () => {
    if (this.state.imgFile) {
      let file = new FormData();
      file.append("image", this.state.imgFile);
      uploadImg(file, (res) => {
        let article = {
          title: this.state.title,
          description: this.state.description,
          category: this.state.categoryId,
          image: res,
        };
        addArticle(article, (msg) => {
          alert(msg);
          this.props.history.push("/");
        });
      });
    } else {
      let article = {
        title: this.state.title,
        description: this.state.description,
        category: this.state.categoryId,
      };
      addArticle(article, (msg) => {
        alert(msg);
        this.props.history.push("/");
      });
    }
  };


  
// -------------------------------------------------------------------
//                  Edit Article
// -------------------------------------------------------------------

  editArticle = () => {
    if (this.state.imgFile) {
      let file = new FormData();
      file.append("image", this.state.imgFile);
      uploadImg(file, (res) => {
        let article = {
          title: this.state.title,
          description: this.state.description,
          category: this.state.categoryId,
          image: res,
        };
        editArticle(article, this.state.updateId, (message) => {
          alert(message);
          this.props.history.push("/");
        });
      });
    } else {
      let article = {
        title: this.state.title,
        description: this.state.description,
        category: this.state.categoryId,
        image: this.state.imgLink,
      };
      editArticle(article, this.state.updateId, (message) => {
        alert(message);
        this.props.history.push("/");
      });
    }
  };



// -------------------------------------------------------------------
//                  Submit
// -------------------------------------------------------------------
  handleSubmit = () => {
    let isValid = this.validation();
    if (isValid) {
      this.state.isUpdate ? this.editArticle() : this.newArticle();
    }
  };


  
// -------------------------------------------------------------------
//                  handle Radio
// -------------------------------------------------------------------
  handleRadio = (e) => {
    this.setState({
      categoryId: e.target.value,
    });
  };


// -------------------------------------------------------------------
//                  handle Change text field
// -------------------------------------------------------------------
  handleChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
      [name]: value,
    });
  };


// -------------------------------------------------------------------
//                  Validation
// -------------------------------------------------------------------
validation = () => {
  let  titleBlank = "";
  let descriptionBlank = "";
  if (!this.state.title) {
    titleBlank = "* Title can not be blank";
  }
  if (!this.state.description) {
    descriptionBlank = "* Description can not be blank";
  }
  if ( titleBlank || descriptionBlank) {
    this.setState({
      titleBlank,
      descriptionBlank,
    });
    return false;
  }
  return true;
};
  render() {
    let categories = this.props.category.map((cat) => (
      <Radio
        key={cat._id}
        category={cat}
        handleRadio={this.handleRadio}
        categoryId={this.state.categoryId}
      />
    ));

    return (
      <div className="container">
        <h1 className="my-4">
          {this.state.isUpdate ? strings.update : strings.add}
        </h1>
        <div className="row">
          <div className="col-md-7">
            <Form>
              <Form.Group controlId="title">
                <Form.Label>{strings.title}</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Input title"
                  value={this.state.title}
                  name="title"
                  onChange={this.handleChange.bind(this)}
                />
                <Form.Text className="text-danger">
                  {this.state.titleBlank}
                </Form.Text>
              </Form.Group>
              <Form.Group>
                <Form.Label className="mr-3">{strings.category} :</Form.Label>
                {categories}
              </Form.Group>
              <Form.Group controlId="description">
                <Form.Label>{strings.description}</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Input Description"
                  value={this.state.description}
                  name="description"
                  onChange={this.handleChange.bind(this)}
                />
                <Form.Text className="text-danger">
                  {this.state.descriptionBlank}
                </Form.Text>
              </Form.Group>
              <Form.Label>{strings.thumbnail}</Form.Label>
              <Form.File
                id="custom-file-translate-scss"
                label="Custom file input"
                lang="en"
                onChange={this.handleChangeImage}
                custom
              />
              <Button
                variant="primary"
                className="my-3"
                onClick={() => this.handleSubmit()}
              >
                {this.state.isUpdate ? "Save" : "Submit"}
              </Button>
            </Form>
          </div>
          <div className="col-md-5">
            <img
              src={
                this.state.imgLink === "" || this.state.imgLink === undefined
                  ? "https://www.slingshotvoip.com/wp-content/uploads/2019/12/placeholder-300x200.png"
                  : this.state.imgLink
              }
              alt="img"
              className="img-fluid"
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    category: store.categoryReducer.category,
  };
};

export default connect(mapStateToProps, { addArticle, getCategory })(AddArticle);
