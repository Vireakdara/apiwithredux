import React from "react";
import { BoxLoading } from "react-loadingg";
import InfiniteScroll from "react-infinite-scroll-component";

function ScrollData({ articles, getMoreData, hasMore }) {
  return (
      <InfiniteScroll
        dataLength={articles.length}
        next={getMoreData}
        hasMore={hasMore}
        loader={<BoxLoading />}
        endMessage={
          <p style={{ textAlign: "center" }}>
            End of Articles
          </p>
        }
      >
        {articles}
      </InfiniteScroll>
  );
}

export default ScrollData;
