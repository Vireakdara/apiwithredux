import {
    GET_ARTICLES,
    GET_MORE_ARTICLES,
    DETELE_ARTICLE,
    SEARCH_ARTICLE,
    FILTER_ARTICLE_BY_CATEGORY,
} from './articleActionType'
import axios from 'axios'
import { baseURL } from '../../../config/url'

// -------------------------------------------------------------------
//                  Get Articlrs
// -------------------------------------------------------------------
export const getArticles=()=>{
    const innnerGetArticles = async (dispatch)=>{
        const result = await axios.get(`${baseURL}/articles?page=1&size=10`)
        dispatch({
            type: GET_ARTICLES,
            payLoad: result.data.data
        })
    }
    return innnerGetArticles
}


// -------------------------------------------------------------------
//                  Get More Articlrs
// -------------------------------------------------------------------
export const  getMoreArticles = (page, cb) => {
    const innnerGetMoreArticles = async (dispatch)=>{
        const result = await axios.get(`${baseURL}/articles?page=${page}&size=10`)
        dispatch({
            type: GET_MORE_ARTICLES,
            payLoad: result.data.data
        })
       cb(result.data.data.length)
    }
    return innnerGetMoreArticles
}


// -------------------------------------------------------------------
//                  delete Articlrs
// -------------------------------------------------------------------
export const deleteArticle = (id) => {
    const innnerDeleteArticle = async (dispatch)=>{
       await axios.delete(`${baseURL}/articles/${id}`)
        dispatch({
            type: DETELE_ARTICLE,
            payLoad: id
        })
    }
    return innnerDeleteArticle
}


// -------------------------------------------------------------------
//                  Add new Articlrs
// -------------------------------------------------------------------
export const addArticle = (article, cb) => {
    axios.post(`${baseURL}/articles`, article).then(res => {
        cb(res.data.message)
    })
}

// -------------------------------------------------------------------
//                  Edit Articlrs
// -------------------------------------------------------------------
export const editArticle = (article, id, cb) => {
    axios.patch(`${baseURL}/articles/${id}`, article).then(res => {
        cb(res.data.message)
    })
}


// -------------------------------------------------------------------
//                  View Articlrs
// -------------------------------------------------------------------
export const viewArticle = (id, cb) => {
    axios.get(`${baseURL}/articles/${id}`).then(res => {
        cb(res.data.data)
    })
}


// -------------------------------------------------------------------
//                  Upload Image into  Articlrs
// -------------------------------------------------------------------
export const uploadImg = (file, cb) => {
    axios.post(`${baseURL}/images`, file).then(res => {
        cb(res.data.url)
    })
}


// -------------------------------------------------------------------
//                  Search Articlrs
// -------------------------------------------------------------------
export const searchArticle = (title) => {
    const innnerSearchArticle = async (dispatch)=>{
        const result = await axios.get(`${baseURL}/articles?title=${title}`)
        dispatch({
            type: SEARCH_ARTICLE,
            payLoad: result.data.data
        })
    }
    return innnerSearchArticle
}


// -------------------------------------------------------------------
//                  Fiter Articlrs by Category
// -------------------------------------------------------------------
export const filterArticleByCategory = (id) => {
    return {
        type: FILTER_ARTICLE_BY_CATEGORY,
        payLoad: id
    }
}
