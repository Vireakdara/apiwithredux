import { combineReducers } from 'redux';
import articleReducer from './article/articleReducer'
import { categoryReducer } from './category/categoryReducer';
const rootReducer = combineReducers({
    articleReducer: articleReducer,
    categoryReducer: categoryReducer
});
export default rootReducer;
